#include "pin.H"
#include <iostream>
#include <string.h>

namespace W {
   #include <windows.h>
   #include <dbghelp.h>
}

FILE * vartrace;
ADDRINT prog;

VOID ValueFromAddress(ADDRINT addr, ADDRINT offset)   // Analysis function, obtains the actual value stored in memory locations
{
   int* value = new int();
   PIN_SafeCopy(value, (VOID *) (addr+offset), sizeof(int));   // Should change so that it accepts an additional size parameter instead of using int
   
   printf("Offset: 0x%08LX\n", offset);
   printf("Address: 0x%08LX\n", addr+offset);
   printf("Value: %d\n", *value);
}

char *strdup (const char *s) {      // Function is used for enlarging the allocated memory for s
    char *d = (char *) (malloc (strlen (s) + 1));   // Allocate memory
    if (d != NULL)
        strcpy (d,s);                    // Copy string if okay
    return d;                            // Return new memory
}

VOID Image(IMG img, VOID* funcSym)
{
   if(IMG_IsMainExecutable(img) && funcSym != NULL)
   {
      for(SYM sym = IMG_RegsymHead(img); SYM_Valid(sym); sym = SYM_Next(sym))    // Loop through all symbols
      {
         if(SYM_Name(sym) == (char*) funcSym)
         {
            prog = SYM_Address(sym);
            break;
         }
      }
      
      RTN rtn = RTN_FindByAddress(prog);     // Tried using RTN_FindByName, but doesn't seem to accept routine names like main
      if(RTN_Valid(rtn))
      {
         RTN_Open(rtn);
         for(INS ins = RTN_InsHead(rtn); INS_Valid(ins); ins = INS_Next(ins))
         {
            fprintf(vartrace, "%s\n", strdup(INS_Disassemble(ins).c_str()));  // Remove if not used - prints each ins in rtn to output file
            if(INS_IsMov(ins) && INS_IsStackWrite(ins))  // Local variables are stored and modified through MOV to stacks
            {
               INS_InsertCall(ins, IPOINT_AFTER, (AFUNPTR) ValueFromAddress,
                     IARG_REG_VALUE, REG_STACK_PTR, IARG_ADDRINT,
                     INS_OperandMemoryDisplacement(ins, 0), IARG_END);
            }
         }
         RTN_Close(rtn);
      }
      
   }
}

int main(int argc, char *argv[])
{
   char* funcSym;
   if(PIN_Init(argc, argv))
   {
      if(argv[3] != "--")        // There should be a better way to do this - checks arg for pintool dll
         funcSym = argv[3];
   }
   
   PIN_InitSymbols();
   vartrace = fopen("vartrace.out", "w");
   
   IMG_AddInstrumentFunction(Image, funcSym);

   PIN_StartProgram();
   return 0;
}
